package ru.t1.oskinea.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

}
