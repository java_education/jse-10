package ru.t1.oskinea.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}
