package ru.t1.oskinea.tm.api.repository;

import ru.t1.oskinea.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

}
