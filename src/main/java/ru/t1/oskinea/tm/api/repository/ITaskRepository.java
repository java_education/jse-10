package ru.t1.oskinea.tm.api.repository;

import ru.t1.oskinea.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

}
