package ru.t1.oskinea.tm.api.service;

import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    Task add(Task task);

    void clear();

    List<Task> findAll();

}
